package hr.apps.mosaic.example_99;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends Activity {

	private static final String MESSAGE = "RMA!\t";
	private static final int MAX_COLOR_RANGE = 256;
	private static final String EMPTY_STRING = "";
	private static final Random mRndGenerator = new Random();

	@BindView(R.id.tvMessage) TextView tvMessage;
	@BindView(R.id.llRoot) LinearLayout llRoot;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ButterKnife.bind(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater menuInflater = this.getMenuInflater();
		menuInflater.inflate(R.menu.main_main, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		switch (item.getItemId()){
			case R.id.menu_add:
				this.addText();
				return true;
			case R.id.menu_change_color:
				this.changeColor();
				return true;
			case R.id.menu_delete:
				this.deleteText();
				return true;
		}
		return false;
	}

	private void deleteText() {
		this.tvMessage.setText(EMPTY_STRING);
	}

	private void changeColor() {
		int r = this.mRndGenerator.nextInt(MAX_COLOR_RANGE);
		int g = this.mRndGenerator.nextInt(MAX_COLOR_RANGE);
		int b = this.mRndGenerator.nextInt(MAX_COLOR_RANGE);
		int color = Color.rgb(r,g,b);
		llRoot.setBackgroundColor(color);
	}

	private void addText() {
		this.tvMessage.append(MESSAGE);
	}
}

